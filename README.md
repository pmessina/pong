# Pong

Project is for Basic Scientific Computing at University of Houston. We
are tasked to do some project using Tcl/Tk. I chose to recreate the game Pong.
Please note that this is the first time I have written anything in Tcl.

# Dependencies
Tcl, Tk.

Img is also used and can be installed on debian based distros by
```Bash
sudo apt-get install libtk-img
```
or if using arch you can find it in the Aur as `tkimg`.

# Usage
* The up arrow key will move the paddle up.
* The down arrow key will move the player down.
* The key q will exit the program.
